package ua.kpi.fpm.pzks.fs;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.concurrent.locks.ReentrantLock;

public class UrlBinaryFile extends BinaryFile {
    private URI uri;

    private UrlBinaryFile(String name, Directory parent, byte[] data, URI uri) {
        super(name, parent, data);
        this.uri = uri;
    }

    public static UrlBinaryFile create(String name, Directory parent, URI uri) throws IOException {
        return new UrlBinaryFile(name, parent, toByteArray(uri), uri);
    }

    public static byte[] toByteArray(final URI uri) throws IOException {

        return IOUtils.toByteArray(uri.toURL());

    }
}
