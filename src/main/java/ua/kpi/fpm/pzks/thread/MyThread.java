package ua.kpi.fpm.pzks.thread;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Phaser;

public abstract class MyThread extends Thread {
    private static Phaser phaseInit = new Phaser();
    private int num;
    protected static CountDownLatch firstTwo = new CountDownLatch(2);
    protected static CountDownLatch lastThread = new CountDownLatch(4);

    public MyThread(int num) {
        this.num = num;
        phaseInit.register();

    }

    @Override
    public void run() {
        task_init();
        phaseOne();
        task();
        phaseTwo();
        task_finalize();
        phaseThree();

    }

    public void phaseOne() {
        phaseInit.arriveAndAwaitAdvance();
    }

    public void phaseThree() {
        lastThread.countDown();
    }

    protected abstract void phaseTwo();

    private void work(String name) {
        System.out.println("Thread " + num + ": started " + name);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thread " + num + ": ended " + name);
    }

    public void task_init() {
        work("task_init");
    }

    public void task() {
        work("task");
    }

    public void task_finalize() {
        work("task_finalize");
    }
}