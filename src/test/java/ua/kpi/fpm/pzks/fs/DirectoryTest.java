package ua.kpi.fpm.pzks.fs;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.kpi.fpm.pzks.fs.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Directory")
public class DirectoryTest {
    @Test
    @DisplayName("Create directory")
    void createDirectoryTest() {

        Directory dir = Directory.create("some", null);
        assertNotNull(dir);

        assertEquals("root", dir.getName());
        Directory subDir = Directory.create("tmp", dir);
        assertEquals("tmp", subDir.getName());
    }

    @Test
    @DisplayName("list directory")
    void listDirectoryTest() {
        Directory root = Directory.create("", null);
        List<Entity> listElements = new ArrayList<Entity>();
        listElements.add(Directory.create("sub", root));
        listElements.add(BinaryFile.create("file.bin", root, "data".getBytes()));
        listElements.add(LogFile.create("log", root, "line"));
        assertEquals(listElements, root.list());
        assertEquals(Collections.emptyList(), Directory.create("", null).list());
    }

    @Test
    @DisplayName("move directory")
    void moveSubdirectoriesAndFilesTest() {
        var root = Directory.create("", null);
        var subDir = Directory.create("folder", root);
        var anotherSubDir = Directory.create("dir", root);

        assertThrows(NullPointerException.class, () -> root.move(subDir));
        anotherSubDir.move(subDir);
        assertTrue(subDir.list().contains(anotherSubDir));
    }

    @Test
    @DisplayName("Add many entities to directory")
    void addManyEntitiesToDirTest() {
        var root = Directory.create("", null);

        for (int i = 0; i < 10; i++) {
            BufferFile.create("name" + i, root);
        }
        assertEquals(10, root.list().size());
    }
}
