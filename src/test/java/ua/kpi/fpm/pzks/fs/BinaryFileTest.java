package ua.kpi.fpm.pzks.fs;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Binary file")
public class BinaryFileTest {
    @Test
    @DisplayName("Create file")
    void createFileTest() {

        var dir = Directory.create("some", null);
        var file = BinaryFile.create("f", dir, "some data".getBytes());
        assertNotNull(file);
        assertEquals("f", file.getName());
    }

    @Test
    @DisplayName("Read bytes")
    void readBytesTest() {
        var dir = Directory.create("d", null);
        var file = BinaryFile.create("f", dir, "some data".getBytes());
        assertArrayEquals("some data".getBytes(), file.read());

        var emptyFile = BinaryFile.create("k", dir, "".getBytes());
        assertEquals(0, emptyFile.read().length);
    }
}
